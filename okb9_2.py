import time

from transacao import open_sap
import re

try:
    # Abrindo a transacao
    session = open_sap("okb9")
    # Pegando a barra inferior para verificar os logs
    sbar = session.findById("wnd[0]/sbar")
    # Selecionando o menu superior de Selecao
    time.sleep(2)
    session.findById("wnd[0]/mbar/menu[3]/menu[0]").select()
    session.findById("wnd[1]/usr/tblSAPLSVIXTCTRL_SEL_FLDS").getAbsoluteRow(4).selected = True
    session.findById("wnd[1]/usr/tblSAPLSVIXTCTRL_SEL_FLDS/txtFIELD_TB-SCRTEXT_L[0,4]").setFocus()
    session.findById("wnd[1]/usr/tblSAPLSVIXTCTRL_SEL_FLDS/txtFIELD_TB-SCRTEXT_L[0,4]").caretPosition = 0
    session.findById("wnd[1]/tbar[0]/btn[0]").press()
    # Inserindo a empresa
    session.findById("wnd[1]/usr/tblSAPLSVIXTCTRL_QUERY/txtQUERY_TAB-BUFFER[3,0]").text = "4000"
    session.findById("wnd[1]/usr/tblSAPLSVIXTCTRL_QUERY/txtQUERY_TAB-BUFFER[3,0]").setFocus()
    session.findById("wnd[1]/usr/tblSAPLSVIXTCTRL_QUERY/txtQUERY_TAB-BUFFER[3,0]").caretPosition = 4
    session.findById("wnd[1]/tbar[0]/btn[8]").press()
    size = int(re.match('.* ([0-9].*)$', session.findById("wnd[0]/usr/txtVIM_POSITION_INFO").text.strip()).group(1))

    # Selecionando o menu de detalhes para cada centro
    # Uso de uma variavel step para rolar a barra lateral
    step = 0
    for i in range(size):
        print(session.findById(f"wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3A/ctxtV_TKA3A-KSTAR[1,{i % 21}]").text)
        session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3A").getAbsoluteRow(i).selected = True
        session.findById(f"wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3A/ctxtV_TKA3A-BUKRS[0,{i % 21}]").setFocus()
        session.findById(f"wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3A/ctxtV_TKA3A-BUKRS[0,{i % 21}]").caretPosition = 0
        session.findById("wnd[0]/shellcont/shell").selectItem("02","Column1")
        session.findById("wnd[0]/shellcont/shell").ensureVisibleHorizontalItem("02","Column1")
        session.findById("wnd[0]/shellcont/shell").doubleClickItem("02","Column1")

        # Inserindo o registro faltante, o 4054, se nao ja houver inserido
        session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C").verticalScrollbar.position = 51
        if '4054' not in session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C/ctxtV_TKA3C-GSBER[1,0]").text:
            # Rolando a barra lateral
            session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C").verticalScrollbar.position = 24
            session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C").getAbsoluteRow(24).selected = True
            session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C/ctxtV_TKA3C-BWKEY[0,0]").setFocus()
            session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C/ctxtV_TKA3C-BWKEY[0,0]").caretPosition = 0
            session.findById("wnd[0]/tbar[1]/btn[6]").press()

            session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C/ctxtV_TKA3C-GSBER[1,0]").text = "4054"
            session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C/ctxtV_TKA3C-KOSTL[2,0]").text = "402114054"
            session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C/ctxtV_TKA3C-KOSTL[2,0]").setFocus()
            session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C/ctxtV_TKA3C-KOSTL[2,0]").caretPosition = 9
            session.findById("wnd[0]").sendVKey(0)
            # Criando a request no primeiro passo do loop
            if i == 0:
                session.findById("wnd[0]/tbar[0]/btn[11]").press()
                session.findById("wnd[1]/tbar[0]/btn[8]").press()
                session.findById("wnd[2]/usr/txtKO013-AS4TEXT").text = "LDANTAS - CRIAÇÃO DA DIVISÃO 4054"
                session.findById("wnd[2]/usr/txtKO013-AS4TEXT").caretPosition = 36
                session.findById("wnd[2]/tbar[0]/btn[0]").press()
                session.findById("wnd[1]/tbar[0]/btn[0]").press()
                session.findById("wnd[0]/tbar[0]/btn[3]").press()
                session.findById("wnd[0]/tbar[0]/btn[3]").press()
        
        session.findById("wnd[0]/tbar[0]/btn[3]").press()
        
        # Rolando a barra lateral
        if (i+1) % 21 == 0:
            session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3A").verticalScrollbar.position = step+21
            step += 21
except Exception as err:
    print(f"Sessão não iniciada! Mensagem de erro -> {err}. Terminando...")
