import win32com.client  # importando a biblioteca de python que implementa a comunicação com objetos COM
import time
import subprocess

def open_sap(transacao: str, username: str, password: str, sapSystem: str, sapClient: str):
    try:
        subprocess.call(f"START sapshcut -user={username} -pw={password} "
                        f"-language=PT -system={sapSystem} -client={sapClient}", shell=1)

        # Tempo crucial para obter a sessão aberta do SAP
        time.sleep(5)

        sap_gui = win32com.client.GetObject("SAPGUI")
        app = sap_gui.GetScriptingEngine
        connection = app.Children(0)
        session = connection.Children(0)

        # Fechando sessão prévia aberta
        close_opened_session(session)

        if 'O nome ou a senha não está correto (repetir o logon)' in session.findById("wnd[0]/sbar").text:
            raise Exception('Usuário e/ou senha inválidos!')

        session.findById("wnd[0]").maximize()

        session.findById("wnd[0]/tbar[0]/okcd").text = "/n" + transacao
        session.findById("wnd[0]").sendVKey(0)

        return session
    except Exception as err:
        raise Exception(f"Erro no acesso inicial ao SAP. {err}")


def close_opened_session(session):

    sbar = session.findById("wnd[0]/sbar")

    if sbar.text == 'Usuário efetuou o logon em outra tela':
        session.findById("wnd[1]/usr/radMULTI_LOGON_OPT1").select()
        session.findById("wnd[1]/usr/radMULTI_LOGON_OPT1").setFocus()
        session.findById("wnd[1]/tbar[0]/btn[0]").press()

def finish_sap_process():

    try:

        subprocess.call(f"taskkill /f /fi \"USERNAME eq %username%\" /im \"saplogon.exe\"", shell=1)
        subprocess.call("taskkill /f /fi \"USERNAME eq %username%\" /im \"saplgpad.exe\"", shell=1)
        time.sleep(2)
        return True

    except:

        return False
