import time, datetime
from openpyxl import load_workbook
import win32clipboard as cp

from fbl3n import FBL3N
from transacao import open_sap, finish_sap_process
import psutil

file, ano = FBL3N()
#file, ano = "C:\\ETL\\Controladoria\\Transacoes\\Arquivos Exportados\\FBL3N\\2021_17.11.2021.xlsx", 2021
if file is not None:
    wb = load_workbook(file)
    ws = wb.active
    # Selecionando os IDs não repetidos da coluna Conta lnçto.contrap.
    lst = []
    for row in range(2, len(ws['M'])):
        if ws[f"M{row}"].value != "":
            lst.append(ws[f"M{row}"].value)
    uniques = list(dict.fromkeys(lst))

    session = open_sap("fbl1n")
    main_window = session.findById("wnd[0]")
    sbar = session.findById("wnd[0]/sbar")

    # Inserindo os IDs únicos na pesquisa
    session.findById("wnd[0]/usr/btn%_KD_LIFNR_%_APP_%-VALU_PUSH").press()
    clip = "\r\n".join(uniques)
    #print(clip)
    cp.OpenClipboard()
    cp.EmptyClipboard()
    cp.SetClipboardText(clip)
    cp.CloseClipboard()
    session.findById("wnd[1]/tbar[0]/btn[24]").press()
    session.findById("wnd[1]/tbar[0]/btn[8]").press()

    # Selecionando Todas as Partidas e determinando o intervalo de data
    session.findById("wnd[0]/usr/radX_AISEL").select()
    session.findById("wnd[0]/usr/ctxtSO_BUDAT-LOW").text = f"01.01.{ano}"
    session.findById("wnd[0]/usr/ctxtSO_BUDAT-HIGH").text = f"31.12.{ano}"
    # Layout /RELATORIO
    session.findById("wnd[0]/usr/ctxtPA_VARI").text = "/RELATORIO"
    # Executando a pesquisa
    session.findById("wnd[0]/tbar[1]/btn[8]").press()

    if "Atenção!" in sbar.text:
        print("FBL1N -> Erro nos parãmetros!")
        exit(1)
    elif "Nenhuma partida selecionada" in sbar.text:
        print(f"FBL1N -> Sem dados para o ano de {ano}!")
        exit(1)

    # A consulta pode demorar a exibir resultados
    while "São exibidas" not in sbar.text:
        time.sleep(1)

    # Exportando para Planilha
    session.findById("wnd[0]/mbar/menu[0]/menu[3]/menu[1]").select()
    session.findById("wnd[1]/tbar[0]/btn[0]").press()
    filepath = "C:\\ETL\\Controladoria\\Transacoes\\Arquivos Exportados\\FBL1N\\"
    session.findById("wnd[1]/usr/ctxtDY_PATH").text = filepath

    filename = f"{ano}_" + datetime.date.today().strftime("%d.%m.%Y")+".xlsx"
    session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = filename

    session.findById("wnd[1]/tbar[0]/btn[11]").press()

    if "Transferir" in sbar.text:
        print("FBL1N -> Salvo com sucesso!")
        # Encerrando os processos Excel abertos
        for proc in psutil.process_iter():
            if proc.name().lower() == "excel.exe":
                proc.kill()
    else:
        print("FBL1N -> Erro na gravação!")
else:
    print("Não foi possível completar a transação FBL1N!")
# Terminando o processo do SAP
finish_sap_process()
