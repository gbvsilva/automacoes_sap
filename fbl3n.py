import datetime, re
import time, psutil

from transacao import open_sap
import os

def FBL3N():
    session = open_sap("fbl3n", os.environ.get('SAP_USERNAME'), os.environ.get('SAP_PASSWORD'), 'GNQ', '400')

    # Obtendo a janela principal e barra inferior de logs
    main_window = session.findById("wnd[0]")
    sbar = session.findById("wnd[0]/sbar")

    # Preenchendo os parametros do Cabecalho
    # Razao de menor identificador
    session.findById("wnd[0]/usr/ctxtSD_SAKNR-LOW").text = "11122001"
    # Razao de maior identificador
    session.findById("wnd[0]/usr/ctxtSD_SAKNR-HIGH").text = "11122043"

    # Selecionando todas as partidas
    session.findById("wnd[0]/usr/radX_AISEL").select()

    # Datas de inicio e fim das partidas
    #ano = input("Digite o ano a ser pesquisado. (atual): ")
    ano = os.environ.get('ANO')
    print(f"Ano -> {ano}")
    if bool(re.match(r"[0-9]+", ano)):
        data = datetime.date.fromisoformat(ano+"-01-01")
    else:
        ano = datetime.date.today().year
        data = datetime.date.fromisoformat(f"{ano}-01-01")

    data_inicial = data.strftime("%d.%m.%Y")
    data_final = datetime.date.fromisoformat(f"{ano}-12-31").strftime("%d.%m.%Y")

    session.findById("wnd[0]/usr/ctxtSO_BUDAT-LOW").text = data_inicial
    session.findById("wnd[0]/usr/ctxtSO_BUDAT-HIGH").text = data_final
    # Layout da saida de listagem
    session.findById("wnd[0]/usr/ctxtPA_VARI").text = "/CONTROL.GN"

    # Executando a pesquisa
    session.findById("wnd[0]/tbar[1]/btn[8]").press()

    if "Atenção!" in sbar.text:
        print("FBL3N -> Erro nos parãmetros!")
        exit(1)
    elif "Nenhuma partida selecionada" in sbar.text:
        print("FBL3N -> Sem dados para o mês "+data_inicial[3:].replace('.', '-')+"!")
        exit(1)

    # A consulta pode demorar a exibir resultados
    while "São exibidas" not in sbar.text:
        time.sleep(1)

    # Selecionando a coluna do montante
    session.findById("wnd[0]/usr/lbl[65,5]").setFocus()
    session.findById("wnd[0]/usr/lbl[65,5]").caretPosition = 5
    session.findById("wnd[0]").sendVKey(2)
    # Botao de filtro
    session.findById("wnd[0]/tbar[1]/btn[38]").press()
    # Selecionando os valores menores que zero do montante
    session.findById("wnd[1]/usr/ssub%_SUBSCREEN_FREESEL:SAPLSSEL:1105/ctxt%%DYN002-LOW").setFocus()
    session.findById("wnd[1]/usr/ssub%_SUBSCREEN_FREESEL:SAPLSSEL:1105/ctxt%%DYN002-LOW").caretPosition = 0
    session.findById("wnd[1]").sendVKey(2)
    session.findById("wnd[2]/usr/cntlOPTION_CONTAINER/shellcont/shell").setCurrentCell(4,"TEXT")
    session.findById("wnd[2]/usr/cntlOPTION_CONTAINER/shellcont/shell").selectedRows = "4"
    session.findById("wnd[2]/tbar[0]/btn[0]").press()
    session.findById("wnd[1]/usr/ssub%_SUBSCREEN_FREESEL:SAPLSSEL:1105/ctxt%%DYN002-LOW").text = "0,00"
    session.findById("wnd[1]/tbar[0]/btn[0]").press()
    # Exportando para Planilha
    session.findById("wnd[0]/mbar/menu[0]/menu[3]/menu[1]").select()
    session.findById("wnd[1]/tbar[0]/btn[0]").press()
    filepath = "C:\\ETL\\Controladoria\\Transacoes\\Arquivos Exportados\\FBL3N\\"
    session.findById("wnd[1]/usr/ctxtDY_PATH").text = filepath

    i = data_inicial.index(".")+1
    filename = f"{ano}_"+datetime.date.today().strftime("%d.%m.%Y")+".xlsx"
    session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = filename
    session.findById("wnd[1]/tbar[0]/btn[11]").press()

    if "Transferir" in sbar.text:
        print("FBL3N -> Salvo com sucesso!")
        # Encerrando os processos Excel abertos
        for proc in psutil.process_iter():
            if proc.name().lower() == "excel.exe":
                proc.kill()
        return filepath+filename, ano
    else:
        print("FBL3N -> Erro na gravação!")
        return None, None

if __name__ == '__main__':
    FBL3N()
