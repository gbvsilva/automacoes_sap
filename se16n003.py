import datetime, time
from transacao import open_sap, finish_sap_process
import psutil

try:
    # Abrindo Transacao
    session = open_sap("se16n")
    tabela = "ZPOSCST003"
    sbar = session.findById("wnd[0]/sbar")
    # Definindo nome da tabela
    session.findById("wnd[0]/usr/ctxtGD-TAB").text = tabela
    session.findById("wnd[0]").sendVKey(0)
    time.sleep(1)
    # Numero maixmo de ocorrencias indefinido
    session.findById("wnd[0]/usr/txtGD-MAX_LINES").text = ""
    # Tipo de operacao
    session.findById("wnd[0]/usr/tblSAPLSE16NSELFIELDS_TC/btnPUSH[4,4]").setFocus()
    session.findById("wnd[0]/usr/tblSAPLSE16NSELFIELDS_TC/btnPUSH[4,4]").press()
    session.findById("wnd[1]/usr/tblSAPLSE16NMULTI_TC/ctxtGS_MULTI_SELECT-LOW[1,0]").text = "4"
    session.findById("wnd[1]/usr/tblSAPLSE16NMULTI_TC/ctxtGS_MULTI_SELECT-LOW[1,1]").text = "11"
    session.findById("wnd[1]/usr/tblSAPLSE16NMULTI_TC/ctxtGS_MULTI_SELECT-HIGH[2,0]").text = "7"
    session.findById("wnd[1]/usr/tblSAPLSE16NMULTI_TC/ctxtGS_MULTI_SELECT-HIGH[2,1]").text = "13"
    session.findById("wnd[1]/tbar[0]/btn[8]").press()
    # Data de operacao = D-1
    hoje = datetime.datetime.now()
    ontem = hoje + datetime.timedelta(days=-1)
    session.findById("wnd[0]/usr/tblSAPLSE16NSELFIELDS_TC/ctxtGS_SELFIELDS-LOW[2,6]").text = ontem.strftime('%d.%m.%Y')
    # Fazendo a consulta
    session.findById("wnd[0]/tbar[1]/btn[8]").press()
    # Exportando planilha
    session.findById("wnd[0]/usr/cntlRESULT_LIST/shellcont/shell").pressToolbarContextButton("&MB_EXPORT")
    session.findById("wnd[0]/usr/cntlRESULT_LIST/shellcont/shell").selectContextMenuItem("&XXL")
    session.findById("wnd[1]/usr/ctxtDY_PATH").text = "C:\\ETL\\SAP"
    session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = tabela+".xlsx"
    session.findById("wnd[1]/tbar[0]/btn[11]").press()

    if "Transferir" in sbar.text:
        print(f"SE16N {tabela} -> Salvo com sucesso!")
        # Encerrando os processos Excel abertos
        for proc in psutil.process_iter():
            if proc.name().lower() == "excel.exe":
                proc.kill()
    else:
        print(f"SE16N {tabela} -> Erro na gravação!")
except Exception as err:
    print(err)
finally:
    finish_sap_process()