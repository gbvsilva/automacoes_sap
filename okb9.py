import time
from tkinter import messagebox
from win32com.client import Dispatch
from transacao import open_sap, finish_sap_process
import re
import os
from tkinter import Tk, Label, StringVar, Entry, Button, messagebox
from functools import partial
import sys


class OKB9():
    def __init__(self, session, username):
        self.session = session
        self.username = username

    def open_empresa(self, empresa):
        self.session.findById("wnd[0]/mbar/menu[3]/menu[0]").select()
        self.session.findById("wnd[1]/usr/tblSAPLSVIXTCTRL_SEL_FLDS").getAbsoluteRow(4).selected = True
        self.session.findById("wnd[1]/usr/tblSAPLSVIXTCTRL_SEL_FLDS/txtFIELD_TB-SCRTEXT_L[0,4]").setFocus()
        self.session.findById("wnd[1]/usr/tblSAPLSVIXTCTRL_SEL_FLDS/txtFIELD_TB-SCRTEXT_L[0,4]").caretPosition = 0
        self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
        # Inserindo a empresa
        self.session.findById("wnd[1]/usr/tblSAPLSVIXTCTRL_QUERY/txtQUERY_TAB-BUFFER[3,0]").text = empresa
        self.session.findById("wnd[1]/usr/tblSAPLSVIXTCTRL_QUERY/txtQUERY_TAB-BUFFER[3,0]").setFocus()
        self.session.findById("wnd[1]/usr/tblSAPLSVIXTCTRL_QUERY/txtQUERY_TAB-BUFFER[3,0]").caretPosition = 4
        #self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
        self.session.findById("wnd[1]/tbar[0]/btn[8]").press()

        # Mapeando todas as entradas
        size = int(re.match('.* ([0-9].*)$', self.session.findById("wnd[0]/usr/txtVIM_POSITION_INFO").text.strip()).group(1))
        hash_map = {}
        offset = 0
        block = 20
        for i in range(size):
            hash_map[self.session.findById(f"wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3A/ctxtV_TKA3A-KSTAR[1,{i % block}]").text] = (i%block, offset)
            if (i+1) % block == 0:
                offset += block
                self.session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3A").verticalScrollbar.position = offset
        self.session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3A").verticalScrollbar.position = 0
        return hash_map

    def open_classe_custo(self, classe_index, classe_offset):
        # Selecionando o menu de detalhes para cada centro do hash_map
        # Talvez não seja preciso rolar a barra lateral aqui devido ao uso do getAbsoluteRow
        self.session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3A").verticalScrollbar.position = classe_offset
        self.session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3A").getAbsoluteRow(classe_offset+classe_index).selected = True
        self.session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3A/ctxtV_TKA3A-BUKRS[0,0]").setFocus()
        self.session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3A/ctxtV_TKA3A-BUKRS[0,0]").caretPosition = 0
        self.session.findById("wnd[0]/shellcont/shell").selectItem("02","Column1")
        self.session.findById("wnd[0]/shellcont/shell").ensureVisibleHorizontalItem("02","Column1")
        self.session.findById("wnd[0]/shellcont/shell").doubleClickItem("02","Column1")

        size = int(re.match('.* ([0-9].*)$', self.session.findById("wnd[0]/usr/txtVIM_POSITION_INFO").text.strip()).group(1))
        offset = 0
        hash_map = {}
        block = 15
        for i in range(size):
            if self.session.findById(f"wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C/ctxtV_TKA3C-PRCTR[4,{i%block}]").text != "":
                self.session.findById(f"wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C/ctxtV_TKA3C-PRCTR[4,{i%block}]").text = ""
                self.session.findById(f"wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C/ctxtV_TKA3C-PRCTR[4,{i%block}]").setFocus()
                self.session.findById(f"wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C/ctxtV_TKA3C-PRCTR[4,{i%block}]").caretPosition = 0
                self.session.findById("wnd[0]").sendVKey(0)
            hash_map[self.session.findById(f"wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C/ctxtV_TKA3C-GSBER[1,{i%block}]").text] = (i%block, offset)
            if (i+1) % block == 0:
                # Rolando a barra lateral
                offset += block
                self.session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C").verticalScrollbar.position = offset
        self.session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C").verticalScrollbar.position = 0
        return hash_map

    def update_centro_custo(self, centro_index, offset, value):
        self.session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C").verticalScrollbar.position = offset
        self.session.findById(f"wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C/ctxtV_TKA3C-KOSTL[2,{centro_index}]").text = value

    def create_centro_custo(self, centro, value):
        size = int(re.match('.* ([0-9].*)$', okb9.session.findById("wnd[0]/usr/txtVIM_POSITION_INFO").text.strip()).group(1))
        self.session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C").verticalScrollbar.position = size-1
        self.session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C").getAbsoluteRow(size-1).selected = True
        self.session.findById("wnd[0]/tbar[1]/btn[6]").press()
        self.session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C/ctxtV_TKA3C-GSBER[1,0]").setFocus()
        self.session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C/ctxtV_TKA3C-GSBER[1,0]").caretPosition = 3
        self.session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C/ctxtV_TKA3C-GSBER[1,0]").text = centro
        self.session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C/ctxtV_TKA3C-KOSTL[2,0]").text = value
        self.session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C/ctxtV_TKA3C-KOSTL[2,0]").setFocus()
        self.session.findById("wnd[0]/usr/tblSAPL0KC4TCTRL_V_TKA3C/ctxtV_TKA3C-KOSTL[2,0]").caretPosition = 9
        self.session.findById("wnd[0]").sendVKey(0)

        # Obtendo a statusBar
        sbar = self.session.findById('wnd[0]/sbar')
        if sbar.messageType == 'W' or sbar.messageType == 'E':
            warning_error_message = sbar.text
            self.session.findById("wnd[0]/tbar[0]/btn[12]").press()
            self.session.findById("wnd[1]/usr/btnSPOP-OPTION1").press()
            raise Exception(warning_error_message)

    def create_request(self):
        self.session.findById("wnd[0]/tbar[0]/btn[11]").press()
        if 'Dados já estavam gravados' not in self.session.findById('wnd[0]/sbar').text:
            self.session.findById("wnd[1]/tbar[0]/btn[8]").press()
            self.session.findById("wnd[2]/usr/txtKO013-AS4TEXT").text = f"{self.username.upper()} - ATUALIZAÇÃO OKB9 INCORPORADAS"
            self.session.findById("wnd[2]/usr/txtKO013-AS4TEXT").caretPosition = 28
            self.session.findById("wnd[2]/tbar[0]/btn[0]").press()
            self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
            return True
        return False

def validate_login(mainWindow, username, password):
    global okb9
    try:
        # Abrindo a transacao
        session = open_sap("okb9", username.get().lower(), password.get(), 'GND', '200')
    except Exception as err:
        finish_sap_process()
        messagebox.showerror("Erro", f"{err} Terminando o SAP...")
    else:
        mainWindow.destroy()
        okb9 = OKB9(session, username.get())

def close_program():
    sys.exit()

# ========= Escopo Main ===========
okb9 = None
username = os.environ.get('SAP_USERNAME')
password = os.environ.get('SAP_PASSWORD')
if not username or not password:
    # Tela de login
    loginWindow = Tk()
    loginWindow.geometry('350x120')
    loginWindow.eval('tk::PlaceWindow . center')
    loginWindow.title('Credenciais do SAP')
    #username label and text entry box
    usernameLabel = Label(loginWindow, text="Usuário SAP", font=("Arial", 12)).grid(row=0, column=0)
    username = StringVar()
    usernameEntry = Entry(loginWindow, textvariable=username).grid(row=0, column=1)
    #password label and password entry box
    passwordLabel = Label(loginWindow, text="Password", font=("Arial", 12)).grid(row=1, column=0)
    password = StringVar()
    passwordEntry = Entry(loginWindow, textvariable=password, show='*').grid(row=1, column=1)
    # validate Login
    validateLogin = partial(validate_login, loginWindow, username, password)
    # login button
    loginButton = Button(loginWindow, text="Login", command=validateLogin).grid(row=4, column=0)
    loginWindow.protocol("WM_DELETE_WINDOW", close_program)
    loginWindow.mainloop()
else:
    session = open_sap("okb9", username.lower(), password, 'GND', '200')
    okb9 = OKB9(session, username)

excel = None
try:
    excel = Dispatch("Excel.Application")
    workbook = excel.Workbooks.Open(os.path.join(os.getcwd(), 'okb9.xlsx'))
    excel.Visible = 1
    workbook.RefreshAll()
    worksheet = workbook.Worksheets('okb9')
except:
    messagebox.showerror("Erro", "Falha na leitura da planilha! Terminando o programa...")
    finish_sap_process()
    if excel:
        excel.Quit()
else:
    # Pegando a barra inferior para verificar os logs
    sbar = okb9.session.findById("wnd[0]/sbar")
    # Selecionando o menu superior de Selecao
    time.sleep(2)
    empresa = ''
    hash_map = None
    classe_custo = ''
    classe_custo_hashmap = None
    for itr in range(worksheet.UsedRange.Rows.Count-1):
        sheet_empresa = worksheet.Range(f"C{itr+2}").Value
        if sheet_empresa and str(int(sheet_empresa)) != empresa:
            empresa = str(int(sheet_empresa))
            hash_map = okb9.open_empresa(empresa)

        sheet_classe_custo = worksheet.Range(f"D{itr+2}").Value
        if sheet_classe_custo and str(int(sheet_classe_custo)) != classe_custo:
            if classe_custo == '':
                # Criando a request no inicio do processo
                okb9.create_request()
            else:
                # Botão para voltar
                okb9.session.findById("wnd[0]/tbar[0]/btn[3]").press()
            classe_custo = str(int(sheet_classe_custo))
            classe_custo_hashmap = okb9.open_classe_custo(*hash_map[classe_custo])
        # Muda a cada iteração
        centro = worksheet.Range(f"E{itr+2}").Value
        if centro:
            centro = str(int(centro))
            try:
                value = worksheet.Range(f"F{itr+2}").Value
                if value and centro in classe_custo_hashmap:
                    okb9.update_centro_custo(*classe_custo_hashmap[centro], str(int(value)))
                elif value:
                    okb9.create_centro_custo(centro, str(int(value)))
            except Exception as err:
                worksheet.Range(f"A{itr+2}").Value = f"Erro. Detalhes: {str(err)}"
            else:
                worksheet.Range(f"A{itr+2}").Value = "OK"
