# Automações SAP
Scripts de automação para processos do SAP

Ativando scripting no SAP:
    Transação rz11 -> Nome do parâmetro=\*gui\*script\* -> Exibir -> Opção sapgui/user_scripting -> Valor Atual=TRUE

Obtendo sessão do SAP:
    Deve ser chamada no escopo global para o SAPGUI ser enxergado.
