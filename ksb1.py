import datetime, time
import sys

from transacao import open_sap, finish_sap_process
import win32clipboard as cp
import psutil

try:
    session = open_sap("ksb1")

    sbar = session.findById("wnd[0]/sbar")
    session.findById("wnd[1]/usr/sub:SAPLSPO4:0300/ctxtSVALD-VALUE[0,21]").text = "1000"
    session.findById("wnd[1]").sendVKey(0)

    if len(sys.argv) > 3:
        data_inicial_mes = sys.argv[3]
        data_final_mes = sys.argv[4]
    else:
        data_inicial_mes = (datetime.datetime.now()+datetime.timedelta(-datetime.date.today().day+1)).strftime("%d.%m.%Y")
        data_final_mes  = (datetime.datetime.now()+datetime.timedelta(31) - datetime.timedelta(datetime.date.today().day+1)).strftime("%d.%m.%Y")
    #print(data_inicial_mes, data_final_mes)

    session.findById("wnd[0]/usr/ctxtR_BUDAT-LOW").text = data_inicial_mes
    session.findById("wnd[0]/usr/ctxtR_BUDAT-HIGH").text = data_final_mes

    session.findById("wnd[0]/usr/ctxtP_DISVAR").text = "/PADRÃO"

    # Inserindo os valores dos centros de custo
    session.findById("wnd[0]/usr/btn%_KOSTL_%_APP_%-VALU_PUSH").press()
    file = open("centros_mkt.txt")
    clip = []
    for line in file.readlines():
        clip.append(line.strip())
    cp.OpenClipboard()
    cp.EmptyClipboard()
    #print("\r\n".join(clip))
    cp.SetClipboardText("\r\n".join(clip))
    cp.CloseClipboard()

    session.findById("wnd[1]/tbar[0]/btn[24]").press()
    session.findById("wnd[1]/tbar[0]/btn[8]").press()
    # Consultando base
    session.findById("wnd[0]/tbar[1]/btn[8]").press()

    # Exportando planilha
    session.findById("wnd[0]/mbar/menu[0]/menu[3]/menu[1]").select()
    session.findById("wnd[1]/tbar[0]/btn[0]").press()

    # Definindo o nome da planilha para adicionar os dados da nova consulta ao final
    session.findById("wnd[1]/usr/ctxtDY_PATH").text = "C:\\ETL\\Marketing\\Transacoes\\Arquivos Exportados\\KSB1"
    session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = "VERBAS MARKETING.xlsx"
    session.findById("wnd[1]/tbar[0]/btn[7]").press()

    if "Transferir" in sbar.text:
        print("KSB1 Parte 1 -> Salvo com sucesso!")
        # Encerrando os processos Excel abertos
        for proc in psutil.process_iter():
            if proc.name().lower() == "excel.exe":
                proc.kill()
    else:
        print("KSB1 Parte 1 -> Erro na gravação!")

    # Voltando e fazendo a Parte 2
    #time.sleep(2)
    session.findById("wnd[0]/tbar[0]/btn[12]").press()

    # Excluindo os codigos dos centros de marketing
    session.findById("wnd[0]/usr/btn%_KOSTL_%_APP_%-VALU_PUSH").press()
    session.findById("wnd[1]/tbar[0]/btn[16]").press()
    session.findById("wnd[1]/tbar[0]/btn[8]").press()

    # Inserindo as 2 classes de custo. Acessoria de Marketing e Taxa de Mídia Regional
    session.findById("wnd[0]/usr/ctxtKSTAR-LOW").text = "41140008"
    session.findById("wnd[0]/usr/ctxtKSTAR-HIGH").text = "41140009"
    # Parâmetros adicionais para a pesquisa
    session.findById("wnd[0]/usr/ctxtKOSTL-LOW").text = "1"
    session.findById("wnd[0]/usr/ctxtKOSTL-HIGH").text = "99999999"
    # Pesquisando...
    session.findById("wnd[0]/tbar[1]/btn[8]").press()

    # Verificando se existem dados lançadas para a data determinada
    try:
        session.findById("wnd[0]/usr/cntlGRID1/shellcont/shell/shellcont[1]/shell").selectedRows = "0"
    except Exception:
        print("KSB1 Parte 2 -> Sem dados lançados!")
        exit(1)
    else:
        # Exportando nova planilha
        session.findById("wnd[0]/mbar/menu[0]/menu[3]/menu[1]").select()
        session.findById("wnd[1]/tbar[0]/btn[0]").press()
        session.findById("wnd[1]/usr/ctxtDY_PATH").text = "C:\\ETL\\Marketing\\Transacoes\\Arquivos Exportados\\KSB1"
        session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = "VERBAS MARKETING 2.xlsx"
        session.findById("wnd[1]/tbar[0]/btn[11]").press()

        if "Transferir" in sbar.text:
            print("KSB1 Parte 2 -> Salvo com sucesso!")
            # Encerrando os processos Excel abertos
            for proc in psutil.process_iter():
                if proc.name().lower() == "excel.exe":
                    proc.kill()
        else:
            print("KSB1 Parte 2 -> Erro na gravação!")
except Exception as err:
    print(f"Sessão não iniciada! Mensagem de erro -> {err}. Terminando...")
finally:
    finish_sap_process()
